# alertmanager-swhook

Alertmanager SNMP Webhook application.  Used to send SNMP traps based on a webhook call from Prometheus Alertmanager.  This is a clean room re-write of an existing application.  [Prometheus Webhook SNMPTrapper](https://github.com/chrusty/prometheus_webhook_snmptrapper)  The existing software has no license included which makes it unusable for many corporations.   I have opened an issue on the original project in the hopes of getting it properly licensed, but in the mean time, I will attempt to "re-write" the application.  It will also use a more modern, maintained SNMP library.

