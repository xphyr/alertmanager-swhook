// alertmanager snmp webhook application
package main

import (
	"log"
	"net/http"

	"github.com/julienschmidt/httprouter"
	"github.com/prometheus/client_golang/prometheus"
	kingpin "gopkg.in/alecthomas/kingpin.v2"
)

var (
	listen_addr = kingpin.Flag("addr", "Address on which to listen").Default(":9099").Envar("SNMP_FORWARDER_ADDRESS").String()
	debug       = kingpin.Flag("debug", "Debug mode").Default("false").Envar("SNMP_FORWARDER_DEBUG").Bool()

	namespace = "forwarder"
	subsystem = "snmp"
	labels    = []string{"topic"}

	snmpRequestsSuccessful = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace: namespace,
			Subsystem: subsystem,
			Name:      "successful_requests_total",
			Help:      "Total number of successful requests to SNMP.",
		},
		labels,
	)

	snmpRequestsUnsuccessful = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Namespace: namespace,
			Subsystem: subsystem,
			Name:      "unsuccessful_requests_total",
			Help:      "Total number of unsuccessful requests to SNMP.",
		},
		labels,
	)
)

func main() {
	kingpin.Parse()

	registerCustomPrometheusMetrics()

	router := httprouter.New()
	router.GET("/health", healthGETHandler)
	router.GET("/metrics", metricsGETHandler)
	router.POST("/alert/:topic", Hello)

	log.Fatal(http.ListenAndServe(listen_addr, router))
}

func registerCustomPrometheusMetrics() {
	prometheus.MustRegister(snsRequestsSuccessful)
	prometheus.MustRegister(snsRequestsUnsuccessful)
}
